//
//  ViewController.swift
//  ls3
//
//  Created by Dmytro Zhylich on 06.04.2021.


import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("Часть 1")
        max(a: 3, b: 9)

        degree(a: 2)

        toMaxToMin(val: 4)

        sumDividers(val: 2)

        superNumber(val: 6)
        superNumber(val: 28)
        superNumber(val: 496)
        superNumber(val: 8128)
        superNumber(val: 23)
        superNumber(val: 622)
        
        print("Часть 2")
        interestRate(dollar: 24, year: 1826, percent: 6)

        studentCosts(uah: 700, spending: 1000, percent: 3)

        richStudent(uah: 2400, scholarship: 700, spending: 1000, percent: 3)

        intRevers(number: 42)
    }
    
    func intRevers(number: Int){
        print("2хзначную цело численную переменную типа \(number). После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед")
        
        var strNumber: String = String(number)
        if strNumber.count != 2 {
            print("Мы так не договаривались, только числа из 2х символов")
            return
        }
        
        var result: String = ""
        for _ in 0..<strNumber.count {
            if strNumber.count > 0 {
                result = result + "\(String(strNumber.popLast()!))"
            }
        }
        
        print("Перевернув число \(number) получим \(result)")
        
        print("************************************\n")
    }
    
    func richStudent(uah: Double, scholarship: Double, spending: Double, percent: Double){
        let text = """
        У студента имеются накопления \(uah) грн. Ежемесячная стипендия составляет \(scholarship) гривен, а расходы на проживание превышают ее и составляют \(spending) грн. в месяц. Рост цен ежемесячно увеличивает расходы на \(percent)%. Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию.
        """
        print(text)

        var resMonths = 0
        var res:Double = uah
        let max_test_months = (99 - 17) * 12
        var spendingNow: Double = spending
        for _ in 1...max_test_months {
            res = res + scholarship - spendingNow
            spendingNow = spendingNow + spendingNow * percent / 100
            
            if res < 0{
                break
            }
            
            resMonths += 1
        }
        
        if resMonths == max_test_months {
            print("Студент очень богат и может прожить всю жизнь так")
        } else {
            print("Студент проживет без голодовки:", resMonths, "месяцев")
        }

        print("************************************\n")
    }
    
    func studentCosts(uah: Double, spending: Double, percent: Double){
        let text = """
        Ежемесячная стипендия студента составляет \(uah) гривен, а расходы на проживание превышают ее и составляют \(spending) грн. в месяц. Рост цен ежемесячно увеличивает расходы на \(percent)%. Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
        """
        print(text)

        let months = 10
        
        var res: Double = 0
        
        var spendingNow: Double = spending
        for _ in 0..<months {
            spendingNow = spendingNow + spendingNow * percent / 100
            res = res + spendingNow - uah
        }
        
        print("Дополнительно нужно иметь чтобы выжить:", Int(res), "гривен")
        print("************************************\n")
    }
        
    func interestRate(dollar: Double, year: Int, percent: Double){
        let text = """
        Остров Манхэттен был приобретен поселенцами за $\(dollar) в \(year) г. Каково было бы в настоящее время состояние их счета, если бы эти \(dollar) доллара были помещены тогда в банк под \(percent)% годового дохода?
        """
        print(text)
        
        let now = 2021
        
        let passed = now - year
        
        var res: Double = dollar
        for _ in 0..<passed {
            res = res + res * percent / 100
        }
        print("Результат: ", Int(res), "$")
        print("************************************\n")
    }
    
    func superNumber(val: Int){
        print("Проверить, является ли заданное число совершенным и найти их (делители) (\(val))")
        
        var dividers = [Int]()
        var summ = 0
        for i in 1..<val{
            if (val % i) == 0{
                summ += i
                dividers.append(i)
            }
        }
        
        if summ == val{
            print("Число - Совершенное!!!")
        } else {
            print("Число - НЕ совершенное")
        }
        print("Делители его: \(dividers)")
        print("Cумма делителей: \(summ)")
        print("Количество делителей: \(dividers.count)")
        
        print("************************************\n")
    }

    func max(a: Int, b: Int){
        print("Вывести на экран наибольшее из двух чисел (\(a) и \(b))")
        
        if a == b {
            print("a == b")
        }

        if a > b {
            print(a)
        } else {
            print(b)
        }
        
        print("************************************\n")
    }

    func degree(a: Int){
        print("Вывести на экран квадрат и куб числа (\(a))")
        
        print(a*a)
        print(a*a*a)
        
        print("************************************\n")
    }

    func toMaxToMin(val: Int){
        print("Вывести на экран все числа до заданного и в обратном порядке до 0 (\(val))")
        
        print("Возрастание    Спадание")
        for i in 0...val {
            print(i, "            ", val-i)
        }
        
        print("************************************\n")
    }

    func sumDividers(val: Int){
        print("Подсчитать общее количество делителей числа и вывести их (\(val))")
        
        var res = 0
        for i in 1...val{
            if (val % i) == 0{
                res += 1
            }
        }
        
        print(res)
        
        print("************************************\n")
    }
}



